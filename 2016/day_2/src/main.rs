use std::fs;
use std::time::Instant;

fn main() {
    let p1_num_pad = vec![vec!['#', '#', '#', '#', '#'],
                          vec!['#', '1', '2', '3', '#'],
                          vec!['#', '4', '5', '6', '#'],
                          vec!['#', '7', '8', '9', '#'],
                          vec!['#', '#', '#', '#', '#']];

    let p2_num_pad = vec![vec!['#', '#', '#', '#', '#', '#', '#'],
                          vec!['#', '#', '#', '1', '#', '#', '#'],
                          vec!['#', '#', '2', '3', '4', '#', '#'],
                          vec!['#', '5', '6', '7', '8', '9', '#'],
                          vec!['#', '#', 'A', 'B', 'C', '#', '#'],
                          vec!['#', '#', '#', 'D', '#', '#', '#'],
                          vec!['#', '#', '#', '#', '#', '#', '#']];

    let input = get_data("data.txt");
    let timer = Instant::now();

    println!("part 1: {}", find_numpad_result(&p1_num_pad, &input));
    println!("part 2: {}", find_numpad_result(&p2_num_pad, &input));

    println!("Time taken: {:.2?}", timer.elapsed())
}

fn get_data(name: &str) -> Vec<String> {
    fs::read_to_string(name).unwrap()
        .split('\n')
        .map(str::trim)
        .filter(|s| !s.is_empty())
        .map(str::to_string)
        .collect::<Vec<_>>()
}

fn find_numpad_result(numpad: &Vec<Vec<char>>, data: &Vec<String>) -> String {
    let mut result = String::new();

    for line in data {
        let mut index = (numpad.len()/2 as usize, numpad[0].len()/2 as usize);
        for character in line.chars() {
            match character {
                'U' => {
                    if numpad[index.1 - 1][index.0] != '#'{
                        index.1 -= 1
                    }
                },
                'D' => {
                    if numpad[index.1 + 1][index.0] != '#'{
                        index.1 += 1
                    }
                },
                'L' => {
                    if numpad[index.1][index.0 - 1] != '#'{
                        index.0 -= 1
                    }
                },
                'R' => {
                    if numpad[index.1][index.0 + 1] != '#'{
                        index.0 += 1
                    }
                },
                _ => {} 
            };
        }
        result.push(numpad[index.1][index.0]);
    }
    result
}
