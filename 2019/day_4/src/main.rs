use std::ops::Index;
use std::time::Instant;
use std::collections::{self, HashMap};

fn main() {
    let numbers_to_check = get_num_vec(128392, 643281);
    let timer = Instant::now();
    let part_1_answer = part_1(&numbers_to_check);
    let end_time = timer.elapsed();
    println!("Part 1 Result: {part_1_answer}");
    println!("Completed in {:.2?}", end_time);
    let timer = Instant::now();
    let part_2_answer = part_2(&numbers_to_check);
    let end_time = timer.elapsed();
    println!("Part 2 Result: {part_2_answer}");
    println!("Completed in {:.2?}", end_time);
}

fn get_num_vec(start: i32, end: i32) -> Vec<String> {
    (start..=end).map(|s| s.to_string())
        .collect::<Vec<_>>()
}

fn part_1(numbers: &Vec<String>) -> i32 {
    let mut result = 0;
    for number in numbers {
        if part_1_number_check(number) {
            result += 1;
        }
    }
    result
}

fn part_1_number_check(number: &String) -> bool {
    let num_as_chars = number.chars();
    let mut last_char = num_as_chars.clone().nth(0).unwrap();
    let mut contains_double = false;
    for character in num_as_chars.skip(1) {
        if character == last_char {
            contains_double = true;
        }
        if character < last_char {
            return false;
        }
        last_char = character;
    }
    contains_double
}

fn part_2(numbers: &Vec<String>) -> i32 {
    let mut result = 0;
    for number in numbers {
        if part_2_number_check(number) {
            println!("{number}");
            result += 1;
        }
    }
    result
}

fn part_2_number_check(number: &String) -> bool {
    let num_as_chars = number.chars();
    let mut last_char = num_as_chars.clone().nth(0).unwrap();
    let mut amount_vector = vec![];
    let mut num_types = vec![];
    for character in num_as_chars.skip(1) {
        if character == last_char {
            if !(num_types.contains(&character)) {
                num_types.push(&character);
                amount_vector.push(0);
            }
            else {
                for i in 0..(num_types.len()-1) {
                    if num_types.get(i).unwrap() == &character {
                        amount_vector[i] += 1;
                        break;
                    }
                }
            }
        }
        if character < last_char {
            return false;
        }
        last_char = character;
    }
    amount_vector.contains(&2)
}