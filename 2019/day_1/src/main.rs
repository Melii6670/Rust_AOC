use std::{fs, str::FromStr, time::Instant};

fn main() {
    let start = Instant::now();
    let mass_vals = get_data("data.txt");
    let mut total_p1 = 0;
    let mut total_p2 = 0;
    for mass in mass_vals {
        total_p1 += calculate_fuel_req_p1(mass);
        total_p2 += calculate_fuel_req_p2(mass);
    }
    let time = start.elapsed();
    println!("Part 1: {}", total_p1);
    println!("Part 2: {}", total_p2);
    println!("Completed in {:.2?}", time);
}

fn get_data(name: &str) -> Vec<i32> {
    fs::read_to_string(name).unwrap()
        .split('\n')
        .map(str::trim)
        .filter(|s| !s.is_empty())
        .map(|s| i32::from_str(s).unwrap())
        .collect::<Vec<_>>()    
}

fn calculate_fuel_req_p1(mass: i32) -> i32 {
    mass/3 - 2
}

fn calculate_fuel_req_p2(mass: i32) -> i32 {
    let mut total_fuel = calculate_fuel_req_p1(mass);
    let mut extra_fuel = total_fuel;
    loop {
        extra_fuel = calculate_fuel_req_p1(extra_fuel);
        if extra_fuel <= 0 {
            break total_fuel
        }
        total_fuel += extra_fuel;
    }
}