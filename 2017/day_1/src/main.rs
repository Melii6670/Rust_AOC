use std::fs; 

fn main() {
    println!("day 1 = {}", day_1("data.txt"));
    println!("day 2 = {}", day_2("data.txt"));
}

fn day_1(filename: &str) -> u32 {
    day_1_get_sum(get_data(filename))
}

fn day_1_get_sum(digits: Vec<u32>) -> u32 {
    let mut sum: u32 = 0;
    for i in 0..digits.len() {
        let current = digits.get(i).unwrap();
        let next = match digits.get(i+1) {
            Some(n2) => {
                n2
            },
            None => {
                digits.get(0).unwrap()
            },
        };
        if current == next {
            sum += current;
        }
    }
    sum
}

fn day_2(filename: &str) -> u32 {
    day_2_get_sum(get_data(filename))
}

fn day_2_get_sum(digits: Vec<u32>) -> u32 {
    let mut sum = 0;
    let half_length = digits.len()/2;
    for i in 0..half_length {
        if digits.get(i).unwrap() == digits.get(i + half_length).unwrap() {
            sum += digits.get(i).unwrap() * 2;
        }
    }
    sum
}

fn get_data(filename: &str ) -> Vec<u32> {
    fs::read_to_string(filename).unwrap()
                            .chars()
                            .map(|c| c.to_digit(10).unwrap())
                            .collect()
}

#[test]
fn test_data_collection() {
    let vec_to_test: Vec<u32> = vec![9,1,1,2,2, 1,1,1,1, 1,2,3,4,1,2,1,2,1,2,9];
    assert_eq!(get_data("testData.txt"), vec_to_test);
}

#[test] 
fn test_day_1_sum_normal() {
    assert_eq!(day_1_get_sum(vec![1,1,2,2]), 3);
}

#[test] 
fn test_day_1_sum_constant() {
    assert_eq!(day_1_get_sum(vec![1,1,1,1]), 4);
}

#[test] 
fn test_day_1_sum_sequential() {
    assert_eq!(day_1_get_sum(vec![1,2,3,4]), 0);
}

#[test] 
fn test_day_1_sum_circular() {
    assert_eq!(day_1_get_sum(vec![9,1,2,1,2,1,2,9]), 9);
}

#[test]
fn day_1_full_test() {
    assert_eq!(day_1("testData.txt"), 16);
}

#[test] 
fn test_day_2_sum_all_matching() {
    assert_eq!(day_2_get_sum(vec![1,2,1,2]), 6);
}

#[test] 
fn test_day_2_sum_no_matching() {
    assert_eq!(day_2_get_sum(vec![1,2,2,1]), 0);
}

#[test] 
fn test_day_2_sum_one_matching_pair() {
    assert_eq!(day_2_get_sum(vec![1,2,3,4,2,5]), 4);
}

#[test] 
fn test_day_2_sum_all_matching_cont() {
    assert_eq!(day_2_get_sum(vec![1,2,3,1,2,3]), 12);
}

#[test] 
fn test_day_2_sum_random() {
    assert_eq!(day_2_get_sum(vec![1,2,1,3,1,4,1,5]), 4);
}
