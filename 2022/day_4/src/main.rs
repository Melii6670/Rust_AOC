use std::fs;
use std::time::Instant;

fn main() {
    let mut timer = Instant::now();

    let data = get_data("data.txt");

    let read_time = timer.elapsed();
    timer = Instant::now();

    let part_1_answer = part_1(&data);

    let part_1_time = timer.elapsed();
    timer = Instant::now();

    let part_2_answer = part_2(&data);

    let part_2_time = timer.elapsed();

    println!("The answer to part 1 is: {part_1_answer}");
    println!("The answer to part 2 is: {part_2_answer}");
    println!("The reading phase took {:.2?}, part 1 took {:.2?} and part 2 took {:.2?}.", read_time, part_1_time, part_2_time);
}

fn part_1(data: &Vec<Vec<u8>>) -> usize {
    let mut sum = 0;
    for line in data {
        if check_if_ranges_contained(&line) {
            sum += 1;
        }
    }
    sum
}

fn check_if_ranges_contained(ranges: &Vec<u8>) -> bool {
    if ranges[0] == ranges[2] {
        return true; 
    }
    if ranges[0] > ranges[2] {
        if ranges[1] <= ranges[3] {
            return true;
        }
    }
    if ranges[3] <= ranges[1] {
        return true;
    }

    false
}

fn part_2(data: &Vec<Vec<u8>>) -> usize {
    let mut sum = 0;
    for line in data {
        if check_for_overlaps(&line) {
            sum += 1;
        }
    }
    sum
}

fn check_for_overlaps(ranges: &Vec<u8>) -> bool {
    if ranges[0] <= ranges[3] && ranges[0] >= ranges[2] {
        return true;
    }
    if ranges[1] <= ranges[3] && ranges[1] >= ranges[2] {
        return true;
    }
    if ranges[0] < ranges[2] && ranges[1] > ranges[3] {
        return true;
    }

    false
}

fn get_data(filename: &str) -> Vec<Vec<u8>> {
    fs::read_to_string(filename).unwrap()
        .lines()
        .map(|s| 
            s.split(|ch| ch == ',' || ch == '-')
            .map(|v| v.parse::<u8>().unwrap())
            .collect::<_>())
        .collect::<Vec<_>>()
}
