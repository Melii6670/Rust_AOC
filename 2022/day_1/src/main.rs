use std::fs;
use std::str::FromStr;
use std::time::Instant;

fn main() {

    let timer = Instant::now();
    //retrieves the total amount of calories for each elf from a text file
    let data = get_total_calorie_data("data.txt");
    
    //finds the part 1 answer and displays it alongside execution time
    println!("The part 1 answer is:");
    println!("{}", part_1(&data));
    
    //finds the part 2 answer and displays it alongside total execution time
    println!("The part 2 answer is:");
    println!("{}", part_2(&data));
    
    println!("Time taken: {:.2?}", timer.elapsed());
}

fn part_1(data: &Vec<usize>) -> usize {
    *(data.iter().max().unwrap())
}

fn part_2(data: &Vec<usize>) -> usize{
    let mut top_elves: [usize; 3] = [0, 0, 0];

    for elf in data {
        if top_elves[0] < *elf {
            replace_lowest_num(&mut top_elves, *elf);
        } 
    }

    return top_elves[0] + top_elves[1] + top_elves[2];
}

//a function which replaces the smallest number in an array while
//still maintaining an ascending order
fn replace_lowest_num(numbers: &mut[usize], new_num: usize ) {
    numbers[0] = new_num;
    for num in 1..(numbers).len() {
        if numbers[num - 1] > numbers[num] {
            let swap = numbers[num];
            numbers[num] = numbers[num - 1];
            numbers[num - 1] = swap;
        }
        else {
            return;
        }
    }
}

//a function which returns a vector of usizes that contains 
//the total calories of each elf as a single value
fn get_total_calorie_data(name: &str) -> Vec<usize> {
    fs::read_to_string(name).unwrap()
        .split("\r\n\r\n")  // /r/n/r/n only for windows - UNIX should need /n/n
        .filter(|s| !s.is_empty())
        .map(|s| {
            s.split("\n")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| usize::from_str(s).unwrap())
            .sum()
        })
        .collect::<_>()
}
