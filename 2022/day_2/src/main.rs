use std::fs;
use std::time::Instant;

fn main() {
    let mut timer = Instant::now();

    let data = get_data_as_plays("data.txt");

    let read_time = timer.elapsed();
    timer = Instant::now();

    //the scores for part 1 where the indexes are the two instructions in each line of the input
    let part_1_scores: [[i32; 3]; 3] = [[4, 8, 3],
                                        [1, 5, 9],
                                        [7, 2, 6]];

    let part_1_answer = score_calc(&data, &part_1_scores);
   
    let part_1_time = timer.elapsed();
    timer = Instant::now();

    //the scores for part 1 where the indexes are the two instructions in each line of the input
    let part_2_scores: [[i32; 3]; 3] = [[3, 4, 8],
                                        [1, 5, 9],
                                        [2, 6, 7]];

    let part_2_answer = score_calc(&data, &part_2_scores);

    let part_2_time = timer.elapsed();

    println!("Part 1's answer is: {}", part_1_answer);
    println!("Part 2's answer is: {}", part_2_answer);
    println!("The time taken for the reading phase was: {:.2?}", read_time);
    println!("The time taken for part 1 was: {:.2?}", part_1_time);
    println!("The time taken for part 2 was: {:.2?}", part_2_time);
}

fn score_calc(data: &Vec<Vec<usize>>, scores: &[[i32; 3]; 3]) -> i32 {
    let mut score = 0;
    for round in data {
        score += scores[round[0]][round[1]];
    }
    score 
}

//a function which retrieves the data from a text file as indexes for a score array
fn get_data_as_plays(name: &str) -> Vec<Vec<usize>> {
    fs::read_to_string(name).unwrap()
        .split("\n")  
        .map(|s| {
            s.split(" ")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| match s {
                "A" | "X" => 0,
                "B" | "Y" => 1,
                "Z" | "C" => 2,
                _ => {panic!("unexpected char");}
            })
            .collect::<_>()
        })
        .collect::<Vec<_>>()
}