use std::fs;
use std::time::Instant;
extern crate queues;
use queues::*;

fn main() {
    let mut timer = Instant::now();
    let data = get_data("data.txt");
    let read_time = timer.elapsed();
    timer = Instant::now();

    let part_1_ans = calculation(&data, 4);
    let part_1_time = timer.elapsed();
    timer = Instant::now();

    let part_2_ans = calculation(&data, 14);
    let part_2_time = timer.elapsed();

    println!("The answer to part 1 is {part_1_ans}");
    println!("The answer to part 2 is {part_2_ans}");
    println!("The reading phase took {:.2?}, part 1 took {:.2?}, part 2 took {:.2?}", read_time, part_1_time, part_2_time);

    timer = Instant::now();
    let part_1_bitwise_ans = part_1_bitwise(&data);
    let bitwise_time = timer.elapsed();

    println!("The answer from my part 1 bitwise solution is {part_1_bitwise_ans} and it took {:.2?}", bitwise_time);
}

fn calculation(data: &Vec<char>, limit: usize) -> usize {
    let mut listing = CharListing {
        char_amounts: [0; 26],
        repeated_amount: 0,
    };
    for i in 0..limit {
        listing.increase_listing(data[i]);
    }
    for i in limit..data.len() {
        listing.decrease_listing(data[i - limit]);
        if listing.increase_listing(data[i]) == 0 {
            return i + 1;
        }
    }
    0
}

fn part_1_bitwise(data: &Vec<char>) -> usize {
    let mut char_queue: Queue<char> = queue![data[0], data[1], data[2], data[3]];
    let mut bit_rep: u32 = 0;
    for i in 0..4 {
        bit_rep ^= char_to_bit_mask(data[i]);
    }
    let mut answer: usize = 0;
    for i in 0..data.len() {
        bit_rep ^= char_to_bit_mask(data[i]);
        char_queue.add(data[i]);
        bit_rep ^= char_to_bit_mask(char_queue.remove().unwrap());
        if bit_rep.count_ones() == 4 {
            answer = i + 1;
            break;
        }
    }
    answer
}

fn char_to_bit_mask(character: char) -> u32{
    (1 as u32) << (character as u32 - 97)
}

#[derive(Debug)]
struct CharListing  {
    char_amounts: [i32; 26],
    repeated_amount: i32,
}

impl CharListing {
    fn increase_listing(&mut self, character: char) -> i32 {
        self.char_amounts[character as usize -97] += 1;
        if self.char_amounts[character as usize - 97] > 1 {
            self.repeated_amount += 1;
        }
        self.repeated_amount
    }

    fn decrease_listing(&mut self, character: char) {
        self.char_amounts[character as usize -97] -= 1;
        if self.char_amounts[character as usize - 97] != 0 {
            self.repeated_amount -= 1;
        }
    }
}


fn get_data(filename: &str) -> Vec<char> {
    fs::read_to_string(filename).unwrap().chars().collect()
}