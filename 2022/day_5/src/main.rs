use std::fs;
use regex::Regex;
use std::time::Instant;

fn main() {
    let mut timer = Instant::now();

    let data = get_data("data.txt");
    let read_time = timer.elapsed();
    timer = Instant::now();

    let part_1_answer = part_1(data.0.clone(), &data.1);
    let part_1_time = timer.elapsed();
    timer = Instant::now();

    let part_2_answer = part_2(data.0.clone(), &data.1);
    let part_2_time = timer.elapsed();

    println!("The answer to part 1 is {}.", part_1_answer);

    println!("The answer to part 2 is {}.", part_2_answer);
    println!("The reading phase took {:.2?}, part 1 took {:.2?} and part 2 took {:.2?}.", read_time, part_1_time, part_2_time);
}

fn part_1(mut stacks: [Vec<char>; 9], instructions: &Vec<(usize, usize, usize)>) -> String {
    let mut transfer: char;
    for instruction in instructions {
        for _ in 0..(instruction.0) {
            transfer = stacks[(instruction.1)-1].pop().unwrap();
            stacks[(instruction.2) - 1].push(transfer);
        }
    }

    let mut answer = String::from("");
    for stack in &mut stacks {
        match stack.last() {
            Some(n) => {answer.push(*n);},
            None => { },
        };
    }
    answer
}

fn part_2(mut stacks: [Vec<char>; 9], instructions: &Vec<(usize, usize, usize)>) -> String {
    for instruction in instructions {
        let mut to_be_appended: Vec<char> = Vec::new();

        for character in &stacks[(instruction.1) - 1][ stacks[(instruction.1 - 1)].len() - instruction.0 .. stacks[(instruction.1 - 1)].len()] {
            to_be_appended.push(*character);
        }

        stacks[instruction.1 - 1].truncate(&stacks[instruction.1 - 1].len() - instruction.0);

        for character in to_be_appended {
            stacks[instruction.2 - 1].push(character);
        }
    }

    let mut answer = String::from("");
    for stack in &mut stacks {
        match stack.last() {
            Some(n) => {answer.push(*n);},
            None => { },
        };
    }
    answer
}

fn get_data(filename: &str) -> ([Vec<char>; 9], Vec<(usize, usize, usize)>) {
    let data = fs::read_to_string(filename).unwrap();
    let mut data_split = data.split("\r\n\r\n")  // /r/n/r/n only for windows - UNIX should need /n/n
                             .filter(|s| !s.is_empty());

    (parse_cargo_from_data(&(data_split.next().unwrap())), parse_instructions_from_data(&(data_split.next().unwrap())))
}

fn parse_instructions_from_data(data: &str) -> Vec<(usize, usize, usize)> {
    let mut cargo_instructions: Vec<(usize, usize, usize)> = Default::default();
    let re = Regex::new(r"[^\d]+").unwrap();
    for line in data.lines() {
        let mut split_line = re.split(line);
        split_line.next();
        cargo_instructions.push((split_line.next().unwrap().parse::<usize>().unwrap(),
                                 split_line.next().unwrap().parse::<usize>().unwrap(),
                                 split_line.next().unwrap().parse::<usize>().unwrap()));
    }

    cargo_instructions                 
}

fn parse_cargo_from_data(data: &str) -> [Vec<char>; 9] {
    let mut cargo_stacks: [Vec<char>; 9] = Default::default();
    for line in data.lines().rev().skip(1) {
        for i in (1..(line.len())).step_by(4) {
            match line.chars().nth(i).unwrap() {
                ' ' => { }
                _ => {cargo_stacks[i/4].push(line.chars().nth(i).unwrap());}
            }
        }
    }
    return cargo_stacks;
}